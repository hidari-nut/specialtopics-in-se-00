<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MahasiswaHasAssignment extends Model
{
    use HasFactory;

    public function assignment()
    {
        return $this->belongsTo('App\Models\Assignment');
    }

    public function mahasiswa()
    {
        return $this->belongsTo('App\Models\Mahasiswa');
    }

    public function fileUpload()
    {
        return $this->hasOne('App\Models\FileUpload');
    }
}
