<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SlotWaktu extends Model
{
    use HasFactory;
    public function ketersediaanRuangan()
    {
        return $this->HasMany('App\Models\KetersediaanRuangan');
    }
}
