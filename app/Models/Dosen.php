<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dosen extends Model
{
    use HasFactory;

    public function dosenHasKeahlian()
    {
        return $this->hasMany('App\Models\DosenHasKeahlian');
    }
    public function pjmk()
    {
        return $this->hasMany('App\Models\Pjmk');
    }
    public function forumDiskusi()
    {
        return $this->hasMany('App\Models\ForumDiskusi');
    }
    public function kelasParalelHasDosen()
    {
        return $this->hasMany('App\Models\KelasParalelHasDosen');
    }
    public function asdos()
    {
        return $this->hasMany('App\Models\Asdos');
    }
    public function absensi()
    {
        return $this->hasMany('App\Models\Absensi');
    }
    public function permintaanTugasAkhir()
    {
        return $this->hasMany('App\Models\PermintaanTugasAkhir');
    }
    public function mahasiswaHasAbsensi()
    {
        return $this->hasMany('App\Models\MahasiswaHasAbsensi');
    }
    public function assignment()
    {
        return $this->hasMany('App\Models\Assignment');
    }
}
