<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Administrasi extends Model
{
    use HasFactory;

    public function ukt_mahasiswa()
    {
        return $this->HasMany('App\Models\UktMahasiswa');
    }

    public function acaraKemahasiswaan()
    {
        return $this->HasMany('App\Models\AcaraKemahasiswaan');
    }
}
