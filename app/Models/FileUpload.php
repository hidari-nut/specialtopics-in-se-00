<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FileUpload extends Model
{
    use HasFactory;

    public function mahasiswaHasAssignment()
    {
        return $this->belongsTo('App\Models\MahasiswaHasAssignment');
    }
}
