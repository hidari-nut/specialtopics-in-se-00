<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DaftarPertemuan extends Model
{
    use HasFactory;

    public function kelasParalel()
    {
        return $this->belongsTo('App\Models\KelasParalel');
    }

    public function absensi()
    {
        return $this->belongsTo('App\Models\Absensi');
    }

    public function forumDiskusi()
    {
        return $this->hasMany('App\Models\ForumDiskusi');
    }

    public function assignment()
    {
        return $this->hasMany('App\Models\Assignment');
    }
}
