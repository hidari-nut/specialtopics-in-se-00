<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ForumDiskusi extends Model
{
    use HasFactory;

    public function daftarPertemuan()
    {
        return $this->belongsTo('App\Models\DaftarPertemuan');
    }

    public function dosen()
    {
        return $this->belongsTo('App\Models\Dosen');
    }

    public function mahasiswa()
    {
        return $this->belongsTo('App\Models\Mahasiswa');
    }
}
