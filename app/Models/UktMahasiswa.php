<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UktMahasiswa extends Model
{
    use HasFactory;

    public function administrasi()
    {
        return $this->belongsTo('App\Models\Administrasi');
    }

    public function jurusan()
    {
        return $this->belongsTo('App\Models\Jurusan');
    }

    public function mahasiswa()
    {
        return $this->belongsTo('App\Models\Mahasiswa');
    }
}
