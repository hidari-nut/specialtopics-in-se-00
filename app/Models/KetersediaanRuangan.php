<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KetersediaanRuangan extends Model
{
    use HasFactory;

    public function Ruangan()
    {
        return $this->belongsTo('App\Models\Ruangan');
    }

    public function SlotWaktu()
    {
        return $this->belongsTo('App\Models\SlotWaktu');
    }

    public function kelasParalel()
    {
        return $this->hasMany('App\Models\KelasParalel');
    }
}
