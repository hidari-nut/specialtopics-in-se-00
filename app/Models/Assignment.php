<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Assignment extends Model
{
    use HasFactory;

    public function daftarPertemuan()
    {
        return $this->belongsTo('App\Models\DaftarPertemuan');
    }

    public function dosen()
    {
        return $this->belongsTo('App\Models\Dosen');
    }

    public function mahasiswa()
    {
        return $this->belongsTo('App\Models\Mahasiswa');
    }

    public function quiz()
    {
        return $this->hasMany('App\Models\Quiz');
    }

    public function mahasiswaHasAssignment()
    {
        return $this->hasMany('App\Models\MahasiswaHasAssignment');
    }
}
