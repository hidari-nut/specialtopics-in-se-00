<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ruangan extends Model
{
    use HasFactory;

    public function gedung()
    {
        return $this->belongsTo('App\Models\Gedung');
    }

    public function propertiRuangan()
    {
        return $this->belongsTo('App\Models\PropertiRuangan');
    }

    public function ketersediaanRuangan()
    {
        return $this->hasMany('App\Models\KetersediaanRuangan');
    }
}
