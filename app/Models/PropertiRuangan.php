<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PropertiRuangan extends Model
{
    use HasFactory;

    public function kategoriProperti()
    {
        return $this->belongsTo('App\Models\KategoriProperti');
    }

    public function ruangan()
    {
        return $this->hasMany('App\Models\Ruangan');
    }
}
