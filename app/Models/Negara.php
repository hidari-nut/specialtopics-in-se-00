<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Negara extends Model
{
    use HasFactory;

    public function provinsi()
    {
        return $this->hasMany('App\Models\Provinsi');
    }
}
