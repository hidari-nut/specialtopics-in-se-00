<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Jurusan extends Model
{
    use HasFactory;

    public function ukt_mahasiswa()
    {
        return $this->HasMany('App\Models\UktMahasiswa');
    }
    public function gedung()
    {
        return $this->HasMany('App\Models\Gedung');
    }
    public function kelasParalel()
    {
        return $this->HasMany('App\Models\KelasParalel');
    }
}
