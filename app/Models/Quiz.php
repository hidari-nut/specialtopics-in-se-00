<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    use HasFactory;

    public function assignment()
    {
        return $this->belongsTo('App\Models\Assignment');
    }

    public function pertanyaan()
    {
        return $this->hasMany('App\Models\Pertanyaan');
    }
}
