<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    use HasFactory;

    public function kota()
    {
        return $this->belongsTo('App\Models\Kota');
    }

    public function orangtua()
    {
        return $this->belongsTo('App\Models\Orangtua');
    }

    public function permintaanTugasAkhir()
    {
        return $this->hasOne('App\Models\PermintaanTugasAkhir');
    }

    public function asdos()
    {
        return $this->hasMany('App\Models\Asdos');
    }

    public function kelasparalelHasMahasiswa()
    {
        return $this->hasMany('App\Models\KelasparalelHasMahasiswa');
    }

    public function acaraKemahasiswaan()
    {
        return $this->hasMany('App\Models\AcaraKemahasiswaan');
    }

    public function uktMahasiswa()
    {
        return $this->hasMany('App\Models\UktMahasiswa');
    }

    public function assignment()
    {
        return $this->hasMany('App\Models\Assignment');
    }

    public function mahasiswaHasAbsensi()
    {
        return $this->hasMany('App\Models\MahasiswaHasAbsensi');
    }

    public function forumDiskusi()
    {
        return $this->hasMany('App\Models\ForumDiskusi');
    }

    public function mahasiswaHasAssignment()
    {
        return $this->hasMany('App\Models\MahasiswaHasAssignment');
    }
}
