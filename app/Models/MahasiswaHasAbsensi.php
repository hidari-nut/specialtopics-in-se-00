<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MahasiswaHasAbsensi extends Model
{
    use HasFactory;

    public function dosen()
    {
        return $this->belongsTo('App\Models\Dosen');
    }

    public function mahasiswa()
    {
        return $this->belongsTo('App\Models\Mahasiswa');
    }

    public function absensi()
    {
        return $this->belongsTo('App\Models\Absensi');
    }
}
