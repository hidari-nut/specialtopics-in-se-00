<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KategoriProperti extends Model
{
    use HasFactory;

    public function propertiRuangan()
    {
        return $this->HasMany('App\Models\PropertiRuangan');
    }
}
