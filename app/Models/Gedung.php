<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gedung extends Model
{
    use HasFactory;

    public function jurusan()
    {
        return $this->belongsTo('App\Models\Jurusan');
    }

    public function ruangan()
    {
        return $this->HasMany('App\Models\Ruangan');
    }
}
