<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Absensi extends Model
{
    use HasFactory;

    public function daftarPertemuan()
    {
        return $this->hasOne('App\Models\DaftarPertemuan');
    }

    public function dosen()
    {
        return $this->belongsTo('App\Models\Dosen');
    }

    public function mahasiswaHasAbsensi()
    {
        return $this->hasMany('App\Models\mahasiswaHasAbsensi');
    }
}
