<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Keahlian extends Model
{
    use HasFactory;

    public function dosenHasKeahlian()
    {
        return $this->hasMany('App\Models\DosenHasKeahlian');
    }
}
