<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pjmk extends Model
{
    use HasFactory;

    public function matakuliah()
    {
        return $this->belongsTo('App\Models\Matakuliah');
    }

    public function semester()
    {
        return $this->belongsTo('App\Models\Semester');
    }

    public function dosen()
    {
        return $this->belongsTo('App\Models\Dosen');
    }
}
