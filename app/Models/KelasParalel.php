<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KelasParalel extends Model
{
    use HasFactory;

    public function matakuliah()
    {
        return $this->belongsTo('App\Models\Matakuliah');
    }

    public function ketersediaanRuangan()
    {
        return $this->belongsTo('App\Models\KetersediaanRuangan');
    }

    public function semester()
    {
        return $this->belongsTo('App\Models\Semester');
    }

    public function jurusan()
    {
        return $this->belongsTo('App\Models\Jurusan');
    }

    public function asdos()
    {
        return $this->belongsTo('App\Models\Asdos');
    }

    public function daftarPertemuan()
    {
        return $this->HasMany('App\Models\DaftarPertemuan');
    }

    public function kelasparalelHasMahasiswa()
    {
        return $this->HasMany('App\Models\KelasparalelHasMahasiswa');
    }
}
