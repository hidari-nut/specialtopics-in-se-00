<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DosenHasKeahlian extends Model
{
    use HasFactory;

    public function keahlian()
    {
        return $this->belongsTo('App\Models\Keahlian');
    }

    public function dosen()
    {
        return $this->belongsTo('App\Models\Dosen');
    }
}
