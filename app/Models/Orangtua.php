<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Orangtua extends Model
{
    use HasFactory;

    public function kota()
    {
        return $this->belongsTo('App\Models\Kota');
    }

    public function mahasiswa()
    {
        return $this->hasOne('App\Models\Mahasiswa');
    }
}
