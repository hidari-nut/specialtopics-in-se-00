<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Matakuliah extends Model
{
    use HasFactory;

    public function kelasParalel()
    {
        return $this->HasMany('App\Models\PropertiRuangan');
    }

    public function pjmk()
    {
        return $this->HasMany('App\Models\Pjmk');
    }
}
