<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kota extends Model
{
    use HasFactory;

    public function provinsi()
    {
        return $this->belongsTo('App\Models\Provinsi');
    }

    public function oranTua()
    {
        return $this->hasMany('App\Models\Orangtua');
    }

    public function mahasiswa()
    {
        return $this->hasMany('App\Models\Mahasiswa');
    }
}
