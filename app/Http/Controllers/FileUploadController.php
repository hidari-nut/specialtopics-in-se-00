<?php

namespace App\Http\Controllers;

use App\Models\FileUpload;
use Illuminate\Http\Request;

class FileUploadController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $fileupload = FileUpload::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('fileupload.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        FileUpload::create($request->all());
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $fileupload = FileUpload::find($id);
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(FileUpload $fileUpload)
    {
        return view('fileupload.edit', compact('fileUpload'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, FileUpload $fileUpload)
    {
        $fileUpload->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $fileupload = FileUpload::find($id);
        $fileupload->delete();
        Session()->flash('message', 'Data has been successfully deleted');
    }
}
