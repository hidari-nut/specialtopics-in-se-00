<?php

namespace App\Http\Controllers;

use App\Models\Orangtua;
use Illuminate\Http\Request;

class OrangtuaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $orangtua = Orangtua::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('orangtua.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        Orangtua::create($request->all());
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $orangtua = Orangtua::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Orangtua $orangtua)
    {
        return view('orangtua.edit', compact('orangtua'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Orangtua $orangtua)
    {
        $orangtua->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $orangtua = Orangtua::find($id);
        $orangtua->delete();
        Session()->flash('message', 'Data has been successfully deleted');
    }
}
