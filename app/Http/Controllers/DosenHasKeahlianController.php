<?php

namespace App\Http\Controllers;

use App\Models\DosenHasKeahlian;
use Illuminate\Http\Request;

class DosenHasKeahlianController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $dosenhaskeahlian = DosenHasKeahlian::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('dosenhaskeahlian.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        DosenHasKeahlian::create($request->all());
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $dosenhaskeahlian = DosenHasKeahlian::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(DosenHasKeahlian $dosenHasKeahlian)
    {
        return view('dosenhaskeahlian.edit', compact('dosenHasKeahlian'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, DosenHasKeahlian $dosenHasKeahlian)
    {
        $dosenHasKeahlian->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $dosenhaskeahlian = DosenHasKeahlian::find($id);
        $dosenhaskeahlian->delete();
        Session()->flash('message', 'Data has been successfully deleted');
    }
}
