<?php

namespace App\Http\Controllers;

use App\Models\DaftarPertemuan;
use Illuminate\Http\Request;

class DaftarPertemuanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $daftarpertemuan = DaftarPertemuan::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('daftarpertemuan.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        DaftarPertemuan::create($request->all());
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $daftarpertemuan = DaftarPertemuan::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(DaftarPertemuan $daftarPertemuan)
    {
        return view('daftarpertemuan.edit', compact('daftarPertemuan'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, DaftarPertemuan $daftarPertemuan)
    {
        $daftarPertemuan->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $daftarpertemuan = DaftarPertemuan::find($id);
        $daftarpertemuan->delete();
        Session()->flash('message', 'Data has been successfully deleted');
    }
}
