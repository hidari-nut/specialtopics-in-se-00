<?php

namespace App\Http\Controllers;

use App\Models\AcaraKemahasiswaan;
use Illuminate\Http\Request;

class AcaraKemahasiswaanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $acarakemahasiswaan = AcaraKemahasiswaan::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('acarakemahasiswaan.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        AcaraKemahasiswaan::create($request->all());
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $acarakemahasiswaan = AcaraKemahasiswaan::find($id);
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(AcaraKemahasiswaan $acaraKemahasiswaan)
    {
        return view('acarakemahasiswaan.edit', compact('acarakemahasiswaan'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, AcaraKemahasiswaan $acaraKemahasiswaan)
    {
        $acaraKemahasiswaan->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $acarakemahasiswaan = AcaraKemahasiswaan::find($id);
        $acarakemahasiswaan->delete();
        Session()->flash('message', 'Data has been successfully deleted');
    }
}
