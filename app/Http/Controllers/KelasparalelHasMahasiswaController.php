<?php

namespace App\Http\Controllers;

use App\Models\KelasparalelHasMahasiswa;
use Illuminate\Http\Request;

class KelasparalelHasMahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $kelasparalelhasmahasiswa = KelasparalelHasMahasiswa::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('kelasparalelhasmahasiswa.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        KelasparalelHasMahasiswa::create($request->all());
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $kelasparalelhasmahasiswa = KelasparalelHasMahasiswa::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(KelasparalelHasMahasiswa $kelasparalelHasMahasiswa)
    {
        return view('kelasparalelhasmahasiswa.edit', compact('kelasparalelHasMahasiswa'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, KelasparalelHasMahasiswa $kelasparalelHasMahasiswa)
    {
        $kelasparalelHasMahasiswa->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $kelasparalelhasmahasiswa = KelasparalelHasMahasiswa::find($id);
        $kelasparalelhasmahasiswa->delete();
        Session()->flash('message', 'Data has been successfully deleted');
    }
}
