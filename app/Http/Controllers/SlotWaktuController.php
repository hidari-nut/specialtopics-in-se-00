<?php

namespace App\Http\Controllers;

use App\Models\SlotWaktu;
use Illuminate\Http\Request;

class SlotWaktuController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $slotwaktu = SlotWaktu::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('slotwaktu.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        SlotWaktu::create($request->all());
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $slotwaktu = SlotWaktu::find($id);
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(SlotWaktu $slotWaktu)
    {
        return view('slotwaktu.edit', compact('slotwaktu'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, SlotWaktu $slotWaktu)
    {
        $slotWaktu->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $slotwaktu = SlotWaktu::find($id);
        $slotwaktu->delete();
        Session()->flash('message', 'Data has been successfully deleted');
    }
}
