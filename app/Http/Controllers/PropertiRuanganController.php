<?php

namespace App\Http\Controllers;

use App\Models\PropertiRuangan;
use GuzzleHttp\Handler\Proxy;
use Illuminate\Http\Request;

class PropertiRuanganController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $propertiruangan = PropertiRuangan::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('propertiruangan.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        PropertiRuangan::create($request->all());
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $propertiruangan = PropertiRuangan::find($id);
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(PropertiRuangan $propertiRuangan)
    {
        return view('propertiruangan.edit', compact('propertiruangan'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, PropertiRuangan $propertiRuangan)
    {
        $propertiRuangan->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $propertiruangan = PropertiRuangan::find($id);
        $propertiruangan->delete();
        Session()->flash('message', 'Data has been successfully deleted');
    }
}
