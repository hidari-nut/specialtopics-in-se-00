<?php

namespace App\Http\Controllers;

use App\Models\KelasParalelHasDosen;
use Illuminate\Http\Request;

class KelasParalelHasDosenController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $kelasparalelhasdosen = KelasParalelHasDosen::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('kelasparalelhasdosen.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        KelasParalelHasDosen::create($request->all());
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $kelasparalelhasdosen = KelasParalelHasDosen::find($id);
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(KelasParalelHasDosen $kelasParalelHasDosen)
    {
        return view('kelasparalelhasdosen.edit', compact('kelasParalelHasDosen'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, KelasParalelHasDosen $kelasParalelHasDosen)
    {
        $kelasParalelHasDosen->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $kelasparalelhasdosen = KelasParalelHasDosen::find($id);
        $kelasparalelhasdosen->delete();
        Session()->flash('message', 'Data has been successfully deleted');
    }
}
