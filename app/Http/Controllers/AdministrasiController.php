<?php

namespace App\Http\Controllers;

use App\Models\Administrasi;
use Illuminate\Http\Request;

class AdministrasiController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $administrasi = Administrasi::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('administrasi.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        Administrasi::create($request->all());
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $administrasi = Administrasi::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Administrasi $administrasi)
    {
        return view('administrasi.edit', compact('administrasi'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Administrasi $administrasi)
    {
        $administrasi->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $administrasi = Administrasi::find($id);
        $administrasi->delete();
        Session()->flash('message', 'Data has been successfully deleted');
    }
}
