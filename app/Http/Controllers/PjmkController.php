<?php

namespace App\Http\Controllers;

use App\Models\Pjmk;
use Illuminate\Http\Request;

class PjmkController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $pjmk = Pjmk::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('pjmk.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        Pjmk::create($request->all());
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $pjmk = Pjmk::find($id);
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Pjmk $pjmk)
    {
        return view('pjmk.edit', compact('pjmk'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Pjmk $pjmk)
    {
        $pjmk->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $pjmk = Pjmk::find($id);
        $pjmk->delete();
        Session()->flash('message', 'Data has been successfully deleted');
    }
}
