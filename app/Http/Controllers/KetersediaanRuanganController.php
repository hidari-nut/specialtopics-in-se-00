<?php

namespace App\Http\Controllers;

use App\Models\KetersediaanRuangan;
use Illuminate\Http\Request;

class KetersediaanRuanganController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $ketersediaanruangan = KetersediaanRuangan::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('ketersediaanruangan.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        KetersediaanRuangan::create($request->all());
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $ketersediaanruangan = KetersediaanRuangan::find($id);
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(KetersediaanRuangan $ketersediaanRuangan)
    {
        return view('ketersediaanruangan.edit', compact('ketersediaanRuangan'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, KetersediaanRuangan $ketersediaanRuangan)
    {
        $ketersediaanRuangan->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $ketersediaanruangan = KetersediaanRuangan::find($id);
        $ketersediaanruangan->delete();
        Session()->flash('message', 'Data has been successfully deleted');
    }
}
