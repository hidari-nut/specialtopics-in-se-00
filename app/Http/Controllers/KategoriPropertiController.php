<?php

namespace App\Http\Controllers;

use App\Models\KategoriProperti;
use Illuminate\Http\Request;

class KategoriPropertiController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $kategoriproperti = KategoriProperti::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('kategoriproperti.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        KategoriProperti::create($request->all());
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $kategoriproperti = KategoriProperti::find($id);
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(KategoriProperti $kategoriProperti)
    {
        return view('kategoriproperti.edit', compact('kategoriProperti'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, KategoriProperti $kategoriProperti)
    {
        $kategoriProperti->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $kategoriproperti = KategoriProperti::find($id);
        $kategoriproperti->delete();
        Session()->flash('message', 'Data has been successfully deleted');
    }
}
