<?php

namespace App\Http\Controllers;

use App\Models\KelasParalel;
use Illuminate\Http\Request;

class KelasParalelController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $kelasparalel = KelasParalel::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('kelasparalel.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        KelasParalel::create($request->all());
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $kelasparalel = KelasParalel::find($id);
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(KelasParalel $kelasParalel)
    {
        return view('kelasparalel.edit', compact('kelasParalel'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, KelasParalel $kelasParalel)
    {
        $kelasParalel->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $kelasparalel = KelasParalel::find($id);
        $kelasparalel->delete();
        Session()->flash('message', 'Data has been successfully deleted');
    }
}
