<?php

namespace App\Http\Controllers;

use App\Models\MahasiswaHasAssignment;
use Illuminate\Http\Request;

class MahasiswaHasAssignmentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $mahasiswahasassignment = MahasiswaHasAssignment::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('mahasiswahasassignment.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        MahasiswaHasAssignment::create($request->all());
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $mahasiswahasassignment = MahasiswaHasAssignment::find($id);
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(MahasiswaHasAssignment $mahasiswaHasAssignment)
    {
        return view('mahasiswahasassignment.edit', compact('mahasiswaHasAssignment'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, MahasiswaHasAssignment $mahasiswaHasAssignment)
    {
        $mahasiswaHasAssignment->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $mahasiswahasassignment = MahasiswaHasAssignment::find($id);
        $mahasiswahasassignment->delete();
        Session()->flash('message', 'Data has been successfully deleted');
    }
}
