<?php

namespace App\Http\Controllers;

use App\Models\Keahlian;
use Illuminate\Http\Request;

class KeahlianController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $keahlian = Keahlian::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('keahlian.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        Keahlian::create($request->all());
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $keahlian = Keahlian::find($id);
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Keahlian $keahlian)
    {
        return view('keahlian.edit', compact('keahlian'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Keahlian $keahlian)
    {
        $keahlian->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $keahlian = Keahlian::find($id);
        $keahlian->delete();
        Session()->flash('message', 'Data has been successfully deleted');
    }
}
