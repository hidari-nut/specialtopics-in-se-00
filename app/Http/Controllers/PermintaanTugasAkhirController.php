<?php

namespace App\Http\Controllers;

use App\Models\PermintaanTugasAkhir;
use Illuminate\Http\Request;

class PermintaanTugasAkhirController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $permintaantugasakhir = PermintaanTugasAkhir::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('permintaantugasakhir.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        PermintaanTugasAkhir::create($request->all());
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $permintaantugasakhir = PermintaanTugasAkhir::find($id);
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(PermintaanTugasAkhir $permintaanTugasAkhir)
    {
        return view('permintaantugasakhir.edit', compact('permintaanTugasAkhir'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, PermintaanTugasAkhir $permintaanTugasAkhir)
    {
        $permintaanTugasAkhir->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $permintaantugasakhir = PermintaanTugasAkhir::find($id);
        $permintaantugasakhir->delete();
        Session()->flash('message', 'Data has been successfully deleted');
    }
}
