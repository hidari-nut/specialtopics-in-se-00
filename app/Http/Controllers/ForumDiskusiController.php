<?php

namespace App\Http\Controllers;

use App\Models\ForumDiskusi;
use Illuminate\Http\Request;

class ForumDiskusiController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $forumdiskusi = ForumDiskusi::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('forumdiskusi.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        ForumDiskusi::create($request->all());
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $forumdiskusi = ForumDiskusi::find($id);
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(ForumDiskusi $forumDiskusi)
    {
        return view('forumdiskusi.edit', compact('forumDiskusi'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, ForumDiskusi $forumDiskusi)
    {
        $forumDiskusi->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $forumdiskusi = ForumDiskusi::find($id);
        $forumdiskusi->delete();
        Session()->flash('message', 'Data has been successfully deleted');
    }
}
