<?php

namespace App\Http\Controllers;

use App\Models\Asdos;
use Illuminate\Http\Request;

class AsdosController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $asdos = Asdos::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('asdos.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        Asdos::create($request->all());
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $asdos = Asdos::find($id);
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Asdos $asdos)
    {
        return view('asdos.edit', compact('asdos'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Asdos $asdos)
    {
        $asdos->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $asdos = Asdos::find($id);
        $asdos->delete();
        Session()->flash('message', 'Data has been successfully deleted');
    }
}
