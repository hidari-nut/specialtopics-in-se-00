<?php

namespace App\Http\Controllers;

use App\Models\MahasiswaHasAbsensi;
use Illuminate\Http\Request;

class MahasiswaHasAbsensiController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $mahasiswahasabsensi = MahasiswaHasAbsensi::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('mahasiswahasabsensi.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        MahasiswaHasAbsensi::create($request->all());
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $mahasiswahasabsensi = MahasiswaHasAbsensi::find($id);
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(MahasiswaHasAbsensi $mahasiswaHasAbsensi)
    {
        return view('mahasiswahasabsensi.edit', compact('mahasiswaHasAbsensi'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, MahasiswaHasAbsensi $mahasiswaHasAbsensi)
    {
        $mahasiswaHasAbsensi->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $mahasiswahasabsensi = MahasiswaHasAbsensi::find($id);
        $mahasiswahasabsensi->delete();
        Session()->flash('message', 'Data has been successfully deleted');
    }
}
