<?php

namespace App\Http\Controllers;

use App\Models\UktMahasiswa;
use Illuminate\Http\Request;

class UktMahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $uktmahasiswa = UktMahasiswa::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('uktmahasiswa.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        UktMahasiswa::create($request->all());
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $uktmahasiswa = UktMahasiswa::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(UktMahasiswa $uktMahasiswa)
    {
        return view('uktmahasiswa.edit', compact('uktmahasiswa'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, UktMahasiswa $uktMahasiswa)
    {
        $uktMahasiswa->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $uktmahasiswa = UktMahasiswa::find($id);
        $uktmahasiswa->delete();
        Session()->flash('message', 'Data has been successfully deleted');
    }
}
