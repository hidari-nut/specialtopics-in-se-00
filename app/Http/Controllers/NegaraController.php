<?php

namespace App\Http\Controllers;

use App\Models\Negara;
use Illuminate\Http\Request;

class NegaraController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $negara = Negara::all();
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('negara.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        Negara::create($request->all());
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        $negara = Negara::find($id);
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Negara $negara)
    {
        return view('negara.edit', compact('negara'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Negara $negara)
    {
        $negara->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $negara = Negara::find($id);
        $negara->delete();
        Session()->flash('message', 'Data has been successfully deleted');
    }
}
