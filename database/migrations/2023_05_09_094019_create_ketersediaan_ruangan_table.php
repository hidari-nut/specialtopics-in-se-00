<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('ketersediaan_ruangan', function (Blueprint $table) {
            $table->unsignedBigInteger('ruangan_id');
            $table->foreign('ruangan_id')->references('id')->on('ruangan');

            $table->unsignedBigInteger('slot_waktu_id');
            $table->foreign('slot_waktu_id')->references('id')->on('slot_waktu');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('ketersediaan_ruangan');
        Schema::table('ketersediaan_ruangan', function (Blueprint $table) {
            $table->dropForeign(['slot_waktu_id']);
            $table->dropColumn('slot_waktu_id');

            $table->dropForeign(['ruangan_id']);
            $table->dropColumn('ruangan_id');
        });
    }
};
