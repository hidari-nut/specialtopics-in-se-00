<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('quiz', function (Blueprint $table) {
            $table->id();
            $table->string("judul");
            $table->enum("tipe_quis", ['essay', 'pilihan ganda']);

            $table->unsignedBigInteger('mahasiswa_id');
            $table->foreign('mahasiswa_id')->references('dosen_id')->on('assignment');

            $table->unsignedBigInteger('dosen_id');
            $table->foreign('dosen_id')->references('mahasiswa_id')->on('assignment');

            $table->unsignedBigInteger('assignment_id');
            $table->foreign('assignment_id')->references('id')->on('assignment');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('quiz');

        Schema::table('quiz', function (Blueprint $table) {
            $table->dropForeign(['dosen_id']);
            $table->dropColumn('dosen_id');

            $table->dropForeign(['mahasiswa_id']);
            $table->dropColumn('mahasiswa_id');

            $table->dropForeign(['assignment_id']);
            $table->dropColumn('assignment_id');
        });
    }
};
