<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('provinsi', function (Blueprint $table) {
            $table->id();
            $table->string("nama");

            $table->unsignedBigInteger('negara_id');
            $table->foreign('negara_id')->references('id')->on('negara');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('provinsi');
        Schema::table('provinsi', function (Blueprint $table) {
            $table->dropForeign(['negara_id']);
            $table->dropColumn('negara_id');
        });
    }
};
