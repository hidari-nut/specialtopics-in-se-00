<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('daftar_pertemuan', function (Blueprint $table) {
            $table->id();
            $table->dateTime("tanggal");
            $table->string("nama");
            $table->string("deskripsi");

            $table->unsignedBigInteger('kelas_paralel_id');
            $table->foreign('kelas_paralel_id')->references('id')->on('kelas_paralel');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('daftar_pertemuan');

        Schema::table('ketersediaan_ruangan', function (Blueprint $table) {
            $table->dropForeign(['kelas_paralel_id']);
            $table->dropColumn('kelas_paralel_id');
        });
    }
};
