<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pertanyaan', function (Blueprint $table) {
            $table->id();
            $table->text("pertanyaan");
            $table->string("jawaban_essasy");
            $table->string("pilihan_ganda_1");
            $table->string("pilihan_ganda_2");
            $table->string("pilihan_ganda_3")->nullable(false);
            $table->string("pilihan_ganda_4")->nullable(false);
            $table->double("nilai");

            $table->unsignedBigInteger('quiz_id');
            $table->foreign('quiz_id')->references('id')->on('quiz');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pertanyaan');

        Schema::table('pertanyaan', function (Blueprint $table) {
            $table->dropForeign(['quiz_id']);
            $table->dropColumn('quiz_id');
        });
    }
};
