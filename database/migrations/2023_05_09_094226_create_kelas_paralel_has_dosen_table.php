<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('kelas_paralel_has_dosen', function (Blueprint $table) {
            $table->unsignedBigInteger('dosen_id');
            $table->foreign('dosen_id')->references('id')->on('dosen');

            $table->unsignedBigInteger('kelas_paralel_id');
            $table->foreign('kelas_paralel_id')->references('id')->on('kelas_paralel');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('kelas_paralel_has_dosen', function (Blueprint $table) {
            $table->dropForeign(['dosen_id']);
            $table->dropColumn('dosen_id');

            $table->dropForeign(['kelas_paralel_id']);
            $table->dropColumn('kelas_paralel_id');
        });

        Schema::dropIfExists('kelas_paralel_has_dosen');
    }
};
