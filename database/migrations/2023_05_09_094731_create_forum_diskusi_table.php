<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('forum_diskusi', function (Blueprint $table) {
            $table->text("komentar");

            $table->unsignedBigInteger('mahasiswa_id')->nullable(false);
            $table->foreign('mahasiswa_id')->references('id')->on('mahasiswa');

            $table->unsignedBigInteger('dosen_id')->nullable(false);
            $table->foreign('dosen_id')->references('id')->on('dosen');

            $table->unsignedBigInteger('daftar_pertemuan_id');
            $table->foreign('daftar_pertemuan_id')->references('id')->on('daftar_pertemuan');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('forum_diskusi');

        Schema::table('forum_diskusi', function (Blueprint $table) {
            $table->dropForeign(['mahasiswa_id']);
            $table->dropColumn('mahasiswa_id');

            $table->dropForeign(['dosen_id']);
            $table->dropColumn('dosen_id');

            $table->dropForeign(['daftar_pertemuan_id']);
            $table->dropColumn('daftar_pertemuan_id');
        });
    }
};
