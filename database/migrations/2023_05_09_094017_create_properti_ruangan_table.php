<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('properti_ruangan', function (Blueprint $table) {
            $table->id();
            $table->string("nama_properti");
            $table->string("kondisi");

            $table->unsignedBigInteger('kategori_properti_id');
            $table->foreign('kategori_properti_id')->references('id')->on('kategori_properti');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('properti_ruangan');

        Schema::table('properti_ruangan', function (Blueprint $table) {
            $table->dropForeign(['kategori_properti_id']);
            $table->dropColumn('kategori_properti_id');
        });
    }
};
