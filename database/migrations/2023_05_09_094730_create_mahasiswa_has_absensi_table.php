<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('mahasiswa_has_absensi', function (Blueprint $table) {
            $table->enum('status', ['absen', 'alfa', 'ijin', 'sakit']);

            $table->unsignedBigInteger('mahasiswa_id');
            $table->foreign('mahasiswa_id')->references('id')->on('mahasiswa');

            $table->unsignedBigInteger('dosen_id')->nullable(false);
            $table->foreign('dosen_id')->references('id')->on('dosen');

            $table->unsignedBigInteger('absensi_id');
            $table->foreign('absensi_id')->references('id')->on('absensi');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('mahasiswa_has_absensi');
        Schema::table('mahasiswa_has_absensi', function (Blueprint $table) {
            $table->dropForeign(['mahasiswa_id']);
            $table->dropColumn('mahasiswa_id');

            $table->dropForeign(['dosen_id']);
            $table->dropColumn('dosen_id');

            $table->dropForeign(['absensi_id']);
            $table->dropColumn('absensi_id');
        });
    }
};
