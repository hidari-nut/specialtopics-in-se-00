<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('ruangan', function (Blueprint $table) {
            $table->id();
            $table->string("nama_ruangan");
            $table->integer("kapasitas");

            $table->unsignedBigInteger('gedung_id');
            $table->foreign('gedung_id')->references('id')->on('gedung');

            $table->unsignedBigInteger('properti_ruangan');
            $table->foreign('properti_ruangan')->references('id')->on('properti_ruangan');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('ruangan');
        Schema::table('ruangan', function (Blueprint $table) {
            $table->dropForeign(['gedung_id']);
            $table->dropColumn('gedung_id');
        });
        Schema::table('ruangan', function (Blueprint $table) {
            $table->dropForeign(['properti_ruangan']);
            $table->dropColumn('properti_ruangan');
        });
    }
};
