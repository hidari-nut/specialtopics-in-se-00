<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('kelas_paralel', function (Blueprint $table) {
            $table->id();
            $table->string("nama");
            $table->integer("kapasitas");
            $table->enum('status_aktif', ['Aktif', 'Tidak Aktif']);

            $table->unsignedBigInteger('mata_kuliah_id');
            $table->foreign('mata_kuliah_id')->references('id')->on('mata_kuliah');

            $table->unsignedBigInteger('jurusan_id');
            $table->foreign('jurusan_id')->references('id')->on('jurusan');

            $table->unsignedBigInteger('semester_id');
            $table->foreign('semester_id')->references('id')->on('semester');

            $table->unsignedBigInteger('ketersediaan_ruangan_id');
            $table->foreign('ketersediaan_ruangan_id')->references('ruangan_id')->on('ketersediaan_ruangan');

            $table->unsignedBigInteger('slot_waktu_id');
            $table->foreign('slot_waktu_id')->references('slot_waktu_id')->on('ketersediaan_ruangan');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('kelas_paralel');

        Schema::table('kelas_paralel', function (Blueprint $table) {
            $table->dropForeign(['mata_kuliah_id']);
            $table->dropColumn('mata_kuliah_id');

            $table->dropForeign(['jurusan_id']);
            $table->dropColumn('jurusan_id');

            $table->dropForeign(['semester_id']);
            $table->dropColumn('semester_id');

            $table->dropForeign(['ketersediaan_ruangan_id']);
            $table->dropColumn('ketersediaan_ruangan_id');

            $table->dropForeign(['slot_waktu_id']);
            $table->dropColumn('slot_waktu_id');
        });
    }
};
