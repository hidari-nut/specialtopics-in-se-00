<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('mahasiswa', function (Blueprint $table) {
            $table->id();
            $table->string("nama_depan");
            $table->string("nama_belakang");
            $table->string("nomor_telepon");
            $table->string("email");
            $table->enum('status_aktif', ['Aktif', 'Tidak Aktif']);
            $table->string("angkatan");
            $table->enum('jenis_kelamin', ['Perempuan', 'Laki-Laki']);
            $table->text("alamat");
            $table->integer("NIK");
            $table->integer("total_sks");
            $table->string("password", 512);
            $table->integer("total_point");
            $table->string("username");

            $table->unsignedBigInteger('kota_asal_id');
            $table->foreign('kota_asal_id')->references('id')->on('kota');

            $table->unsignedBigInteger('orang_tua_id');
            $table->foreign('orang_tua_id')->references('id')->on('orangtua');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('mahasiswa');

        Schema::table('mahasiswa', function (Blueprint $table) {
            $table->dropForeign(['kota_asal_id']);
            $table->dropColumn('kota_asal_id');

            $table->dropForeign(['orang_tua_id']);
            $table->dropColumn('orang_tua_id');
        });
    }
};
