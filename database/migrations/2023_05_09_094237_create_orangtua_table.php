<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orangtua', function (Blueprint $table) {
            $table->id();
            $table->integer("nomor_telepon");
            $table->string("nama_ayah");
            $table->string("nama_ibu");
            $table->text("alamat");

            $table->unsignedBigInteger('kota_asal_id');
            $table->foreign('kota_asal_id')->references('id')->on('kota');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orangtua');
        Schema::table('orangtua', function (Blueprint $table) {
            $table->dropForeign(['kota_asal_id']);
            $table->dropColumn('kota_asal_id');
        });
    }
};
