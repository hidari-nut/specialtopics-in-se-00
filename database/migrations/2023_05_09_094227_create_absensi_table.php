<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('absensi', function (Blueprint $table) {
            $table->id();
            $table->dateTime("tanggal");

            $table->unsignedBigInteger('daftar_pertemuan_id');
            $table->foreign('daftar_pertemuan_id')->references('id')->on('daftar_pertemuan');

            $table->unsignedBigInteger('dosen_id');
            $table->foreign('dosen_id')->references('id')->on('dosen');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('absensi');

        Schema::table('absensi', function (Blueprint $table) {
            $table->dropForeign(['daftar_pertemuan_id']);
            $table->dropColumn('daftar_pertemuan_id');

            $table->dropForeign(['dosen_id']);
            $table->dropColumn('dosen_id');
        });
    }
};
