<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('acara_kemahasiswaan', function (Blueprint $table) {
            $table->id();
            $table->string("nama_acara");
            $table->integer("point");

            $table->unsignedBigInteger('administrasi_id');
            $table->foreign('administrasi_id')->references('id')->on('administrasi');

            $table->unsignedBigInteger('mahasiswa_id');
            $table->foreign('mahasiswa_id')->references('id')->on('mahasiswa');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('acara_kemahasiswaan');

        Schema::table('acara_kemahasiswaan', function (Blueprint $table) {
            $table->dropForeign(['administrasi_id']);
            $table->dropColumn('administrasi_id');

            $table->dropForeign(['mahasiswa_id']);
            $table->dropColumn('mahasiswa_id');
        });
    }
};
