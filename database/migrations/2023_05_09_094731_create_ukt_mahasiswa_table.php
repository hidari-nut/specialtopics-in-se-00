<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('ukt_mahasiswa', function (Blueprint $table) {
            $table->id();
            $table->integer("nominal");
            $table->dateTime("jatuh_tempo");

            $table->unsignedBigInteger('mahasiswa_id')->nullable(false);
            $table->foreign('mahasiswa_id')->references('id')->on('mahasiswa');

            $table->unsignedBigInteger('jurusan_id')->nullable(false);
            $table->foreign('jurusan_id')->references('id')->on('jurusan');

            $table->unsignedBigInteger('administrasi_id');
            $table->foreign('administrasi_id')->references('id')->on('administrasi');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('ukt_mahasiswa');
        Schema::table('ukt_mahasiswa', function (Blueprint $table) {
            $table->dropForeign(['mahasiswa_id']);
            $table->dropColumn('mahasiswa_id');

            $table->dropForeign(['jurusan_id']);
            $table->dropColumn('jurusan_id');

            $table->dropForeign(['administrasi_id']);
            $table->dropColumn('administrasi_id');
        });
    }
};
