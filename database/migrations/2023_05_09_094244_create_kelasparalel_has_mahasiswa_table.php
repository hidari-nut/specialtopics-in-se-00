<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('kelasparalel_has_mahasiswa', function (Blueprint $table) {
            $table->unsignedBigInteger('kelas_paralel_id');
            $table->foreign('kelas_paralel_id')->references('id')->on('kelas_paralel');

            $table->unsignedBigInteger('mahasiswa_id');
            $table->foreign('mahasiswa_id')->references('id')->on('mahasiswa');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('kelasparalel_has_mahasiswa', function (Blueprint $table) {
            $table->dropForeign(['kelas_paralel_id']);
            $table->dropColumn('kelas_paralel_id');

            $table->dropForeign(['mahasiswa_id']);
            $table->dropColumn('mahasiswa_id');
        });

        Schema::dropIfExists('kelasparalel_has_mahasiswa');
    }
};
